import serial
import time
import aqi
from Adafruit_IO import Client
from datetime import datetime

# adafruit username and key
aio = Client('sc19ahw', 'aio_erwl45QyPjEryNFj0moDNuqaiQxH')

# connect to usb port on raspberry pi
ser=serial.Serial('/dev/ttyUSB0')

# saves data to csv file    
def save_log():
    with open("/home/pi/readings.csv", "a") as log:
        dt = datetime.now()
        log.write("Date {},PM2.5{},AQI_OF_PM2.5{},PM10{},AQI_OF_PM10{} \n".format(dt, pmtwofive, aqi_2_5, pmten,aqi_10))
        log.close()

 # method to convert reading to equivalent aqi values       
def conv_aqi(pwtwofive, pmten):
    try:
        aqi_2_5 = aqi.to_iaqi(aqi.POLLUTANT_PM25, str(pmtwofive), algo=aqi.ALGO_EPA)
        aqi_10 = aqi.to_iaqi(aqi.POLLUTANT_PM10, str(pmten),algo=aqi.ALGO_EPA)
        return aqi_2_5, aqi_10
    except:
        return 600,600

 # while program is running, execute this code       
while True:
    
    
    data=[]
    for index in range(0,10):
        datum=ser.read()
        data.append(datum)
     
    # used to get the pm2.5 values from sensor   
    pmtwofive = int.from_bytes(b''.join(data[2:4]), byteorder = 'little')/10
    #sends data to adafruit account, first variable is the name of the adafruit feed
    aio.send('air-quality-monitor', pmtwofive)
    pmten = int.from_bytes(b''.join(data[4:6]), byteorder ='little')/10
    aio.send('air-quality-monitor-10', pmten)
    aqi_2_5, aqi_10 = conv_aqi(pmtwofive, pmten)
    #sleep for 15 seconds
    time.sleep(15)
    save_log()

        
